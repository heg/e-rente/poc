import axios from 'axios'
// import { setupCache } from 'axios-cache-interceptor'
// setupCache(axios, {
//   cache: {
//     ttl: 1000
//   }
// })

export default (context, inject) => {
  const basePath = `${context.$config.dibsApiBasePath}`
  let cancelSource

  const api = {
    get: (endpoint, params, watched = false, ttl = 500) => {
      cancelSource = axios.CancelToken.source()

      const query = new URLSearchParams()
      for (const param in params) {
        if (typeof params[param] === 'string') {
          query.set(param, params[param])
        } else {
          query.set(param, JSON.stringify(params[param]))
        }
      }

      return new Promise((resolve, reject) => {
        axios
          .get(`${basePath}${endpoint}?${query.toString()}`, {
            withCredentials: true,
            cancelToken: cancelSource.token,
            watched,
            cache: {
              ttl
            }
          })
          .then(function (data) {
            return resolve(data.data)
          })
          .catch(function (e) {
            return reject(e)
          })
      })
    },
    cancelGet (msg = 'cancelled') {
      if (cancelSource) {
        cancelSource.cancel(msg)
      }
    },
    delete: (endpoint, watched = false) => {
      return axios.delete(`${basePath}${endpoint}`, {
        withCredentials: true,
        watched
      })
    },
    put: (endpoint, params, watched = false) => {
      return axios.put(`${basePath}${endpoint}`, params, {
        withCredentials: true,
        watched
      })
    },
    patch: (endpoint, params, watched = false) => {
      return axios.patch(`${basePath}${endpoint}`, params, {
        withCredentials: true,
        watched
      })
    },
    post: (endpoint, params, queries = {}, watched = false) => {
      const query = new URLSearchParams()
      for (const param in queries) {
        if (typeof queries[param] === 'string') {
          query.set(param, queries[param])
        } else {
          query.set(param, JSON.stringify(queries[param]))
        }
      }

      return axios.post(`${basePath}${endpoint}?${query.toString()}`, params, {
        withCredentials: true,
        watched
      })
    }
  }

  inject('api', api)
}
