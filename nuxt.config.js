export default {
  server: {
    port: process.env.SERVER_PORT || 4001, // par défaut: 3001
    host: process.env.SERVER_HOST || 'localhost' // par défaut: localhost
  },
  publicRuntimeConfig: {
    dibsApiBasePath: process.env.API_BASE_PATH,
    ver: process.env.npm_package_version ? process.env.npm_package_version : null
  },
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'erente-website',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }

    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://cdn.jsdelivr.net/gh/yegor256/tacit@gh-pages/tacit-css.min.css' },
      { rel: 'stylesheet', href: 'https://use.typekit.net/fpb8dgo.css' }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '@/node_modules/vue-loading-overlay/dist/vue-loading.css',
    '@/node_modules/vue-multiselect/dist/vue-multiselect.min.css',
    '~/static/style.css'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '~/plugins/Api.js'
    // "~/plugins/EventBus.js",
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios'
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    // Workaround to avoid enforcing hard-coded localhost:3000: https://github.com/nuxt-community/axios-module/issues/308
    baseURL: '/'
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  }
}
