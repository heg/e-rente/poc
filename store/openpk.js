export const state = () => ({
  registry: null,
  policies: null
})

export const mutations = {
  setRegistry (state, data) {
    state.registry = data
  },
  setPolicies (state, data) {
    state.policies = data
  }
}
