export const state = () => ({
  login: null,
  fund_ids: null
})

export const mutations = {
  setLogin (state, data) {
    state.login = data
  },
  setFunds (state, data) {
    state.fund_ids = data
  },
  emptyLogin (state) {
    state.login = null
  }
}
