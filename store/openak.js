export const state = () => ({
  navs: null,
  birthdate: null,
  calculator: null
})

export const mutations = {
  setNavs (state, data) {
    state.navs = data
  },
  setBirthdate (state, data) {
    state.birthdate = data
  },
  setCalculator (state, data) {
    state.calculator = data
  }
}
