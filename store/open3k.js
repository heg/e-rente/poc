export const state = () => ({
  solde: null,
  amount: null,
  rate: null,
  paydate: null
})

export const mutations = {
  setInfo (state, data) {
    state.solde = data.solde;
    state.amount = data.amount;
    state.rate = data.rate;
    state.paydate = data.paydate;
  },
  setSolde (state, data) {
    state.solde = data
  },
  setAmount (state, data) {
    state.amount = data
  },
  setRate (state, data) {
    state.rate = data
  },
  setPaydate (state, data) {
    state.paydate = data
  }
}
